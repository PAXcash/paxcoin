
Debian
====================
This directory contains files used to package paxd/pax-qt
for Debian-based Linux systems. If you compile paxd/pax-qt yourself, there are some useful files here.

## pax: URI support ##


pax-qt.desktop  (Gnome / Open Desktop)
To install:

	sudo desktop-file-install pax-qt.desktop
	sudo update-desktop-database

If you build yourself, you will either need to modify the paths in
the .desktop file or copy or symlink your paxqt binary to `/usr/bin`
and the `../../share/pixmaps/pax128.png` to `/usr/share/pixmaps`

pax-qt.protocol (KDE)

