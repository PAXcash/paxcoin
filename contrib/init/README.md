Sample configuration files for:

SystemD: paxd.service
Upstart: paxd.conf
OpenRC:  paxd.openrc
         paxd.openrcconf
CentOS:  paxd.init

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
